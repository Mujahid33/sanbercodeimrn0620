import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TextInput, TouchableOpacity } from 'react-native';

export default class LoginScreen extends Component {
  state = {
    username: '',
    password: ''
  }

  handleUsername = (text) => {
    this.setState({ username: text })
  }
  handlePassword = (text) => {
    this.setState({ password: text })
  }

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container} >
        <Image source={require('./assets/logo.png')} style={{ width: 200, height: 60, marginBottom: 50 }} />

        <Text style={{ textAlign: 'center', fontSize: 17, marginBottom: 50 }}>Login</Text>

        <Text style={{ textAlign: 'left', fontSize: 13 }}>Username / Email</Text>

        <TextInput style={styles.input}
          onChangeText={this.handleUsername} />

        <Text style={{ textAlign: 'left', fontSize: 13 }}>Password</Text>

        <TextInput style={styles.input}
          onChangeText={this.handlePassword} />

        <TouchableOpacity
          style={styles.SubmitButtonStyle1}
          activeOpacity={.5}
          onPress={() => navigation.navigate("About")}
        >

          <Text style={{ textAlign: 'center', color: 'white' }}> Masuk</Text>

        </TouchableOpacity>

        <Text style={{ color: '#3EC6FF', textAlign: 'center', marginTop: 6 }}>atau</Text>

        <TouchableOpacity
          style={styles.SubmitButtonStyle}
          activeOpacity={.5}
        >

          <Text style={{ textAlign: 'center', color: 'white' }}> Daftar ?</Text>

        </TouchableOpacity>



      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'center'
  },
  input: {
    height: 40,
    borderColor: '#000',
    borderWidth: 1,
    marginBottom: 10,
    padding: 10
  },
  SubmitButtonStyle: {
    marginTop: 10,
    backgroundColor: '#003366',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',

  },
  SubmitButtonStyle1: {
    marginTop: 10,
    backgroundColor: '#3EC6FF',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff'
  },
})
