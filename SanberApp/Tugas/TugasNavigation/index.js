import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Login from './LoginScreen';
import About from './AboutScreen';
import Skill from './SkillScreen';
import Add from './AddScreen';
import Project from './ProjectScreen';

const Tabs = createBottomTabNavigator();
const LoginStack = createStackNavigator();
const Drawer = createDrawerNavigator();

const SkillStack = createStackNavigator();
const ProjectStack = createStackNavigator();
const AddStack = createStackNavigator();
const AboutStack = createStackNavigator();

const SkillStackScreen = () => (
  <SkillStack.Navigator>
    <SkillStack.Screen name="Skill" component={Skill} />
  </SkillStack.Navigator>
)
const ProjectStackScreen = () => (
  <ProjectStack.Navigator>
    <ProjectStack.Screen name="Project" component={Project} />
  </ProjectStack.Navigator>
)
const AddStackScreen = () => (
  <AddStack.Navigator>
    <AddStack.Screen name="Add" component={Add} />
  </AddStack.Navigator>
)

const TabsScreen = () => (

  <Tabs.Navigator>
    <Tabs.Screen name="Skill" component={SkillStackScreen} />
    <Tabs.Screen name="Project" component={ProjectStackScreen} />
    <Tabs.Screen name="Add" component={AddStackScreen} />
  </Tabs.Navigator>
);

const AboutStackScreen = () => (
  <AboutStack.Navigator>
    <AboutStack.Screen name="About" component={About} />
  </AboutStack.Navigator>
)

const DrawerScreen = () => (

  <Drawer.Navigator>
    <Drawer.Screen name="About" component={AboutStackScreen} />
    <Drawer.Screen name="Skills" component={TabsScreen} />

  </Drawer.Navigator>
)


export default () => (
  <NavigationContainer>
    <LoginStack.Navigator>
      <LoginStack.Screen name="Login" component={Login} />
      <LoginStack.Screen name="About" component={DrawerScreen} />
    </LoginStack.Navigator>
  </NavigationContainer>
)