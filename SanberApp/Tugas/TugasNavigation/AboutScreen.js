import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TextInput, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import { AntDesign } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';

export default class AboutScreen extends Component {
  render() {
    return (
      <View style={styles.container} >
        <ScrollView>



          <Text style={{ fontSize: 25, color: '#003366', fontWeight: 'bold' }}>Tentang Saya</Text>


          <Icon name="account-circle" size={200} color="#EFEFfF" />

          <Text style={{ marginTop: 5, fontSize: 17, color: '#003366', fontWeight: 'bold' }}>Mujahid Islami Primaldi. A</Text>
          <Text style={{ marginTop: 5, fontSize: 12, color: '#3EC6FF', fontWeight: 'bold', }}>Web Developer</Text>

          <View style={{ marginTop: 20, width: 300, backgroundColor: "#EFEFEF", borderRadius: 10, padding: 10 }}>
            <Text>Portofolio</Text>
            <View style={{ borderBottomColor: 'black', borderBottomWidth: 1, marginBottom: 10 }} />

            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 5 }}>

              <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginLeft: 20 }}>
                <Icon2 name="gitlab" color="#3EC6FF" size={30} />
                <Text style={{ color: "#003366", fontSize: 10, fontWeight: 'bold', marginTop: 3 }}>@Mujahid33</Text>
              </View>

              <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginRight: 20 }}>
                <AntDesign name="github" size={30} color="#3EC6FF" />
                <Text style={{ color: "#003366", fontSize: 10, fontWeight: 'bold', marginTop: 3 }}>Mujahid33</Text>
              </View>
            </View>
          </View>


          <View style={{ marginTop: 20, width: 300, backgroundColor: "#EFEFEF", borderRadius: 10, padding: 10 }}>
            <Text>Hubungi Saya</Text>
            <View style={{ borderBottomColor: 'black', borderBottomWidth: 1, marginBottom: 10 }} />

            <View style={{ flex: 1, flexDirection: 'column', paddingBottom: 5, justifyContent: 'center', marginBottom: 10 }}>

              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 10 }}>
                <Entypo name="facebook" size={30} color="#3EC6FF" />
                <Text style={{ color: "#003366", fontSize: 10, fontWeight: 'bold', marginTop: 3, marginLeft: 10 }}>Mujahid Islami</Text>
              </View>

              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 10 }}>
                <AntDesign name="instagram" size={30} color="#3EC6FF" />
                <Text style={{ color: "#003366", fontSize: 10, fontWeight: 'bold', marginTop: 3, marginLeft: 10 }}>@mujahidipa</Text>
              </View>

              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <Entypo name="twitter" size={30} color="#3EC6FF" />
                <Text style={{ color: "#003366", fontSize: 10, fontWeight: 'bold', marginTop: 3, marginLeft: 10 }}>@IpaMujahid</Text>
              </View>
            </View>
          </View>

        </ScrollView>
      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
})
