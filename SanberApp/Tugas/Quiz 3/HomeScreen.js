import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';

import data from './data.json';

const DEVICE = Dimensions.get('window')

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
      totalPrice: 0,
      //saya menambah state dibawah untuk menampung data-data dari json
      jsonData: []
    }
  }

  //saya menggunakan component did mount untuk mengisi data-data dari json ke state yang sudah saya buat
  componentDidMount() {
    this.setState({ jsonData: data.produk });
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  updatePrice(price) {
    //? #Soal Bonus (10 poin) 
    //? Buatlah teks 'Total Harga' yang akan bertambah setiap kali salah satu barang/item di klik/tekan.
    //? Di sini, buat fungsi untuk menambahkan nilai dari state.totalPrice dan ditampilkan pada 'Total Harga'.

    // Kode di sini
  }


  render() {
    console.log(data)
    const { route } = this.props;
    return (
      <View style={styles.container}>
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>Hai,{'\n'}
              {/* //? #Soal 1 Tambahan, Simpan userName yang dikirim dari halaman Login pada komponen Text di bawah ini */}
              <Text style={styles.headerText}>{route.params.name}</Text>
            </Text>

            {/* //? #Soal Bonus, simpan Total Harga dan state.totalPrice di komponen Text di bawah ini */}
            <Text style={{ textAlign: 'right' }}>Total Harga{'\n'}
              <Text style={styles.headerText}>tempat total harga</Text>
            </Text>
          </View>
          <View>

          </View>
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder='Cari barang..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>

        {/* 
        //? #Soal No 2 (15 poin)
        //? Buatlah 1 komponen FlatList dengan input berasal dari data.json
        //? dan pada prop renderItem menggunakan komponen ListItem -- ada di bawah --
        //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal)

        // Lanjutkan di bawah ini!
        */}
        <FlatList
          data={this.state.jsonData}
          renderItem={({ item }) => {
            return (
              <View style={{ flex: 2, flexDirection: 'row', backgroundColor: 'white', justifyContent: 'center', marginBottom: 20 }}>

                <View style={{ flexDirection: 'column', justifyContent: 'center' }}>

                  <Image source={{ uri: item.gambaruri }} style={{ width: 150, height: 150 }} />

                  <Text style={{ fontWeight: 'bold', textAlign: 'center', marginBottom: 20 }}>
                    {item.nama}
                  </Text>

                  <Text style={{ color: 'blue', fontWeight: 'bold', textAlign: 'center' }}>
                    {this.currencyFormat(parseInt(item.harga))}
                  </Text>

                  <Text style={{ textAlign: 'center' }}>
                    Sisa stok: {item.stock}
                  </Text>

                  <TouchableOpacity style={{ backgroundColor: 'blue', borderRadius: 5, paddingBottom: 10, paddingTop: 10 }}>
                    <Text style={{ color: 'white', textAlign: 'center' }}>Beli</Text>

                  </TouchableOpacity>


                </View>
              </View>
            );
          }} />
      </View>
    )
  }
};

class ListItem extends React.Component {

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  //? #Soal No 3 (15 poin)
  //? Buatlah styling komponen ListItem, agar dapat tampil dengan baik di device

  render() {
    const data = this.props.data
    return (
      <View style={styles.itemContainer}>
        <Image source={{ uri: data.gambaruri }} style={styles.itemImage} resizeMode='contain' />
        <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.nama}</Text>
        <Text style={styles.itemPrice}>{this.currencyFormat(Number(data.harga))}</Text>
        <Text style={styles.itemStock}>Sisa stok: {data.stock}</Text>
        <Button title='BELI' color='blue' />
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    width: DEVICE.width * 0.44,
  },
  itemImage: {
  },
  itemName: {
  },
  itemPrice: {
  },
  itemStock: {
  },
  itemButton: {
  },
  buttonText: {
  }
})
