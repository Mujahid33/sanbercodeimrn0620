import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Login from './components/LoginScreen';
import About from './components/AboutScreen';


export default function App() {
  return (

    <View style={styles.container}>
      <Login />
      <About />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center'
  },
});

// export default App;
