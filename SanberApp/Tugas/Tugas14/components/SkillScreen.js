import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TextInput, TouchableOpacity, FlatList } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Ionicons } from '@expo/vector-icons';
import skill from '../skillData.json';

export default class Skill extends Component {

  constructor(props) {
    super(props);
    this.state = {
      skillData: []
    }
  }

  componentDidMount() {
    this.setState({
      skillData: skill.items
    });
  }


  render() {
    return (
      <View style={styles.container}>

        <View style={styles.logo}>
          <Image source={require('../assets/logo.png')} style={styles.logoImg} />
        </View>

        <View style={styles.name}>

          <View style={{ flexDirection: 'row' }}>

            <MaterialIcons name="account-circle" size={30} color="#3EC6FF" />

            <View style={{ flexDirection: 'column', marginLeft: 5 }}>

              <Text style={{ fontSize: 10, fontWeight: 'bold' }}>Hai,</Text>
              <Text style={{ color: '#003366', fontSize: 13, fontWeight: 'bold' }}>Mujahid Islami Primaldi. A</Text>
            </View>

          </View>
        </View>

        <Text style={{ marginTop: 15, marginLeft: 10, fontSize: 30, color: '#003366' }}>SKILL</Text>

        <View style={{ borderBottomColor: 'B4E9FF', borderBottomWidth: 3, margin: 10 }} />

        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginRight: 10, marginLeft: 10 }}>

          <TouchableOpacity
            style={styles.btn}
            activeOpacity={.5}
          >

            <Text style={{ textAlign: 'center', color: '#003366', fontWeight: 'bold' }}>Library / Framework</Text>

          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btn}
            activeOpacity={.5}
          >

            <Text style={{ textAlign: 'center', color: '#003366', fontWeight: 'bold' }}>Bahasa Pemrograman</Text>

          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btn}
            activeOpacity={.5}
          >

            <Text style={{ textAlign: 'center', color: '#003366', fontWeight: 'bold' }}>Teknologi</Text>

          </TouchableOpacity>
        </View>

        <FlatList
          data={this.state.skillData}
          renderItem={({ item }) => {

            return (
              <View style={styles.flat}>

                <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                  <Icon name={item.iconName} size={90} color="#003366" />

                </View>

                <View style={{ flexDirection: 'column' }}>

                  <Text style={{ fontSize: 20, fontWeight: 'bold', color: "#003366", marginBottom: 5 }}>
                    {item.skillName}
                  </Text>

                  <Text style={{ fontSize: 15, color: "#3EC6FF" }}>
                    {item.categoryName}
                  </Text>

                  <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                    <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 40 }}>
                      {item.percentageProgress}
                    </Text>
                  </View>
                </View>



                <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                  <Ionicons name="ios-arrow-forward" size={90} color="#003366" />

                </View>
              </View>
            );
          }}
        />

      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'center'
  },
  logo: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  logoImg: {
    width: 200,
    height: 60,
    marginBottom: 50
  },
  name: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginLeft: 10
  },
  btn: {
    backgroundColor: '#B4E9FF',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    padding: 8
  },
  item: {
    backgroundColor: '#3EC6FF',
    padding: 10,
    margin: 10
  },
  flat: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#B4E9FF',
    margin: 10,
    borderRadius: 10,
    padding: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
  }
})