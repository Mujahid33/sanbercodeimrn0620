//Soal nomor 1 (Array to Object)
console.log("Jawaban soal nomor 1:")

function arrayToObject(arr) {
  // Code di sini 
  var objPeople = {};

  var now = new Date();
  var thisYear = now.getFullYear();

  let i = 1;

  arr.forEach(element => {
    objPeople.firstName = element[0];
    objPeople.lastName = element[1];
    objPeople.gender = element[2];

    if (element[3] === undefined || element[3] > thisYear) {
      objPeople.age = "Invalid Birth Year";
    }
    else {
      objPeople.age = thisYear - element[3];
    }

    console.log(i + ". " + objPeople.firstName + " " + objPeople.lastName + ":")
    console.log(objPeople)

    i++;

  });

  console.log()
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
/*
  1. Bruce Banner: { 
      firstName: "Bruce",
      lastName: "Banner",
      gender: "male",
      age: 45
  }
  2. Natasha Romanoff: { 
      firstName: "Natasha",
      lastName: "Romanoff",
      gender: "female".
      age: "Invalid Birth Year"
  }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
  1. Tony Stark: { 
      firstName: "Tony",
      lastName: "Stark",
      gender: "male",
      age: 40
  }
  2. Pepper Pots: { 
      firstName: "Pepper",
      lastName: "Pots",
      gender: "female".
      age: "Invalid Birth Year"
  }
*/

// Error case 
arrayToObject([]) // ""

console.log()

//Soal nomor 2 (Shopping Time)
console.log("Jawaban soal nomor 2:")
function shoppingTime(memberId, money) {
  // you can only write your code here!
  let sale = {
    'Sepatu Stacattu': 1500000,
    'Baju Zoro': 500000,
    'Baju H&N': 250000,
    'Sweater Uniklooh': 175000,
    'Casing Handphone': 50000
  }

  let purchasedData = {};
  let listPurchased = [];
  let notEnough = "Mohon maaf, uang tidak cukup";
  let changeMoney = money;

  if (memberId === undefined || memberId === '') {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  }
  else {

    for (let [key, value] of Object.entries(sale)) {
      if (changeMoney >= value) {
        listPurchased.push(key);
        changeMoney -= value;
      }
      else {
      }
    }

    if (listPurchased.length > 0) {
      purchasedData = {
        memberId: memberId,
        money: money,
        listPurchased: listPurchased,
        changeMoney: changeMoney
      }

      return purchasedData;

    }
    else {
      return notEnough;
    }
  }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log()

//soal nomor 3 (Naik Angkot)
console.log('Jawaban soal nomor 3:')

function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  //your code here

  let returnList = [];
  let tmp;

  if (arrPenumpang.length === 0) {
    return arrPenumpang;
  }
  else {
    arrPenumpang.forEach(data => {
      let purchase = 0;
      let start = rute.indexOf(data[1]);
      let finish = rute.indexOf(data[2]);

      for (let index = start; index < finish; index++) {

        purchase = purchase + 2000;

      }
      returnList.push({
        penumpang: data[0],
        naikDari: data[1],
        tujuan: data[2],
        bayar: purchase
      });


    });

    return returnList;
  }
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]