//soal nomor 1 (range)
function range(startNum, finishNum) {
  let arr = [];

  if (startNum === undefined || finishNum === undefined) {
    return -1;
  }
  else {

    if (startNum > finishNum) {
      for (let index = startNum; index >= finishNum; index--) {
        arr.push(index);
      }

    }
    else {

      for (let index = startNum; index <= finishNum; index++) {
        arr.push(index);
      }
    }

    return arr;
  }
}

console.log('Jawaban soal nomor 1:');

console.log();
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log()

//soal nomor 2
function rangeWithStep(startNum, finishNum, step) {

  let arr = [];
  if (startNum > finishNum) {
    for (let index = startNum; index >= finishNum; index -= step) {
      arr.push(index);
    }

  }
  else {

    for (let index = startNum; index <= finishNum; index += step) {
      arr.push(index);
    }
  }

  return arr;
}

console.log('Jawaban soal nomor 2:');

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

console.log()

//soal nomor 3
function sum(startNum, finishNum, step) {
  let sumOfRange = 0;

  if (finishNum === undefined && step === undefined) {
    if (startNum == undefined) {
      return 0;
    }
    else {
      return startNum;
    }
  }

  if (step === undefined) {

    if (startNum > finishNum) {
      for (let index = startNum; index >= finishNum; index--) {
        sumOfRange += index;
      }

    }
    else {

      for (let index = startNum; index <= finishNum; index++) {
        sumOfRange += index;
      }
    }
  }
  else {
    if (startNum > finishNum) {
      for (let index = startNum; index >= finishNum; index -= step) {
        sumOfRange += index;
      }

    }
    else {

      for (let index = startNum; index <= finishNum; index += step) {
        sumOfRange += index;
      }
    }
  }

  return sumOfRange;
}

console.log('Jawaban soal nomor 3');

console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0

console.log();

//soal nomor 4
function dataHandling(arr) {
  arr.forEach(data => {
    console.log('Nomor id: ' + data[0]);
    console.log('Nama Lengkap: ' + data[1]);
    console.log('TTL: ' + data[2] + ' ' + data[3]);
    console.log('Hobi: ' + data[4]);
    console.log();
  })
}

console.log('Jawaban soal nomor 4');

var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling(input);

console.log();

//soal nomor 5
function balikKata(word) {
  let reverseString = '';

  for (let index = word.length; index > -1; index--) {

    reverseString += word.charAt(index);
  }

  return reverseString;

}

console.log('Jawaban soal nomor 5');

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log();

//soal nomor 6
function dataHandling2(input) {
  let newName = input[1].concat('Elsharawy');
  let newProvince = "Provinsi " + input[2];

  input.splice(1, 1, newName);
  input.splice(2, 1, newProvince);
  input.splice(4, 1, "Pria", "SMA Internasional Metro");
  console.log(input)

  let dateFromInput = input[3].split('/');
  month = Number(dateFromInput[1]);

  switch (month) {
    case 1:
      console.log('Januari')
      break;
    case 2:
      console.log('Februari')
      break;
    case 3:
      console.log('Maret')
      break;
    case 4:
      console.log('April')
      break;
    case 5:
      console.log('Mei')
      break;
    case 6:
      console.log('Juni')
      break;
    case 7:
      console.log('Juli')
      break;
    case 8:
      console.log('Agustus')
      break;
    case 9:
      console.log('September')
      break;
    case 10:
      console.log('Oktober')
      break;
    case 11:
      console.log('November')
      break;
    case 12:
      console.log('Desember')
      break;
    default:
      console.log('Bulan tidak ada')
      break;
  }

  let joinDate = dateFromInput.join('-');

  dateFromInput.sort(function (a, b) { return b - a })
  console.log(dateFromInput);
  console.log(joinDate);

  console.log(input[1].slice(0, 15))



  console.log()
}

console.log('Jawaban soal nomor 6');

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);