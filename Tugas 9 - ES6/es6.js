//soal nomor 1 ubah fungsi menjadi arrow function
console.log('Jawaban soal nomor 1')
const golden = () => {
  console.log("this is golden!!")
}

golden()

console.log()

//soal nomor 2 sederhanakan menjadi object literal di es6
console.log('Jawaban soal nomor 2')

const newFunction = (firstName, lastName) => ({
  firstName,
  lastName,
  fullName: () => {
    console.log(firstName + " " + lastName)
    return
  }
})

//Driver Code 
newFunction("William", "Imoh").fullName()

console.log()

//soal nomor 3 destructing
console.log('Jawaban soal nomor 3')

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject

// Driver code
console.log(firstName, lastName, destination, occupation)

console.log()

//soal nomor 4 array spreading
console.log('Jawaban soal nomor 4')

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

// const combined = west.concat(east)

const combined = [...west, ...east]
//Driver Code
console.log(combined)

console.log()

//soal nomor 5 template literals
console.log('Jawaban soal nomor 5')
const planet = "earth"
const view = "glass"
// var before = 'Lorem ' + view + 'dolor sit amet, ' +
//   'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//   'incididunt ut labore et dolore magna aliqua. Ut enim' +
//   ' ad minim veniam'

var before = `Lorem ${view} dolor sit amet,` +
  `consectetur adipiscing elit, ${planet} do eiusmod tempor ` +
  `incididunt ut labore et dolore magna aliqua. Ut enim ` +
  `ad minim veniam`

// Driver Code
console.log(before)