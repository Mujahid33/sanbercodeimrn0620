//Nomor 1 looping while
console.log('Soal nomor 1 looping while');

var i = 2;
console.log('LOOPING PERTAMA');

while (i <= 20) {
  console.log(i + ' - I love coding');
  i += 2;
}

i -= 2;
console.log('LOOPING KEDUA');

while (i >= 2) {
  console.log(i + ' - I will become a mobile developer');
  i -= 2;
}

console.log();

//Nomor 2 looping for
console.log('Soal nomor 2 looping for');

for (var i = 1; i <= 20; i++) {
  if (i % 2 !== 0) {
    if (i % 3 === 0) {
      console.log(i + ' - I Love Coding');
    }
    else {
      console.log(i + ' - Santai');
    }
  }
  else {
    console.log(i + ' - Berkualitas');
  }
}
console.log();

//Nomor 3 Persegi Panjang
console.log('Soal nomor 3 persegi panjang');

var square = '';

for (var i = 1; i <= 4; i++) {
  for (var j = 1; j <= 8; j++) {
    square += '#';
  }
  console.log(square);
  square = '';
}

console.log();

//Nomor 4 Membuat Tangga
console.log('Soal nomor 4 membuat tangga');

var stairs = '';

for (var i = 1; i <= 7; i++) {
  for (var j = 1; j <= i; j++) {
    stairs += '#';
  }
  console.log(stairs);
  stairs = '';
}

console.log();

//Nomor 5 Membuat Papan Catur
console.log('Soal nomor 5 membuat papan catur');

var chessBoard = '';

for (var i = 1; i <= 8; i++) {
  for (var j = 1; j <= 8; j++) {
    if (i % 2 !== 0) {
      if (j % 2 !== 0) {
        chessBoard += ' ';
      }
      else {
        chessBoard += '#'
      }
    }
    else {
      if (j % 2 === 0) {
        chessBoard += ' ';
      }
      else {
        chessBoard += '#';
      }
    }
  }
  console.log(chessBoard);
  chessBoard = '';
}